package ir.behzadnikbin.crawler.service.crawlservices.store.sheypoor;

import java.util.List;
import java.util.Map;

public class SheypoorBrowseCategoryResponseDto {
    public static class FeaturesClass {
        public static class CategoryClass {
            public Boolean hasMore;

            public static class ItemClass {
                public String id;
            }

            public List<ItemClass> items;
        }

        public CategoryClass CATEGORY;
    }

    public FeaturesClass features;

    public static class ProductClass {
        public String item;
        public String title;
        public Boolean available;
        public String brand;
        public Long price;
        public String category;
        public Long msrp;
        public String link;
        public String image;
        public String zoom_image;
    }

    public Map<String, ProductClass> products;
}
