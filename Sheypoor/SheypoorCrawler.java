package ir.behzadnikbin.crawler.service.crawlservices.store.sheypoor;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.jaunt.NotFound;
import com.jaunt.UserAgent;
import ir.behzadnikbin.crawler.model.crawl.StoreCategory;
import ir.behzadnikbin.crawler.model.crawl.StoreProduct;
import ir.behzadnikbin.crawler.model.crawl.StoreProductReview;
import ir.behzadnikbin.crawler.model.crawl.StoreProductSpecification;
import ir.behzadnikbin.crawler.utility.ScrapUtils;
import ir.behzadnikbin.crawler.utility.UrlUtils;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

@Slf4j
@Component
public class SheypoorCrawler {

    @Autowired
    private ScrapUtils scrapUtils;

    @Value("${ir.behzadnikbin.crawler.crawl-timeout}")
    private int timeout;

    @PostConstruct
    public void init() {
    }

    public String getStoreProductDetailsByJava(StoreProduct storeProduct) {
        String url = UrlUtils.urlEncode(storeProduct.getUrl());
        log.info("crawling {}", url);
        UserAgent userAgent = null;
        String imgSrc = null;
        try {
            userAgent = new UserAgent();
            userAgent.cookies.empty();
            userAgent.settings.responseTimeout = timeout;
            userAgent.settings.checkSSLCerts = false;

            com.jaunt.Document doc = userAgent.visit(url);
            //  short description //not available for Sheypoor
            /*
            com.jaunt.Element shortDescElement = findFirst(doc, "<ul data-title='ویژگی\u200Cهای محصول'>");
            if (shortDescElement != null) {
                storeProduct.setShortDescription(shortDescElement.getTextContent());
            }*/
            //  description
            com.jaunt.Element descElement = findFirst(doc, "<p class='description'>");
            if (descElement != null) {
                storeProduct.setDescription(descElement.getTextContent());
            }
            //  brand //not available for Sheypoor
            /*
            com.jaunt.Element brandElement = findFirst(doc, "<a class='btn-link-spoiler product-brand-title'>");
            if (brandElement != null) {
                storeProduct.setBrand(brandElement.getTextContent());
            }
			*/
            //  specifications	//not available for Sheypoor
            /*
            com.jaunt.Element specContainer = findFirst(doc, "<div class=c-params>");
            if (specContainer != null) {
                List<StoreProductSpecification> specs = new ArrayList<>();
                com.jaunt.Elements specGroup = findEach(specContainer, "<ul class=c-params__list>");
                for (com.jaunt.Element group : specGroup) {
                    com.jaunt.Elements rows = findEach(group, "<li>");
                    for (com.jaunt.Element row : rows) {
                        //  title
                        com.jaunt.Element keyElement = findFirst(row, "<div class=c-params__list-key>");
                        //  value
                        com.jaunt.Element valueElement = findFirst(row, "<div class=c-params__list-value>");

                        String key = keyElement == null ? null : keyElement.getTextContent();
                        String value = valueElement == null ? null : valueElement.getTextContent();
                        specs.add(new StoreProductSpecification(storeProduct, key, value));
                    }
                }
                storeProduct.setSpecifications(specs);
            }*/

            //  image
            com.jaunt.Element gallerySectionElement = findFirst(doc, "<div class=slick-slide slick-current slick-active>");
            if (gallerySectionElement != null) {
                com.jaunt.Element galleryDivElement = findFirst(gallerySectionElement, "<figure class=ratio-16-10>");
                if (galleryDivElement != null) {
                    com.jaunt.Element imgElement = findFirst(galleryDivElement, "img");
                    if (imgElement != null) {
                        imgSrc = getAt(imgElement, "src");
                    }
                }
            }

            //  category path (breadcrumb)
            com.jaunt.Element breadcrumb = findFirst(doc, "<nav id=breadcrumbs>");
            if (breadcrumb != null) {
                storeProduct.setCategoryPath(breadcrumb.getTextContent());
            }

            List<StoreProductReview> reviews = getReviews(userAgent, storeProduct);
            storeProduct.setReviews(reviews);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (userAgent != null) {
                try {
                    userAgent.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return imgSrc;
    }

//not available for Sheypoor
/*
    public List<StoreProductReview> getReviews(UserAgent userAgent, StoreProduct storeProduct) {
        log.debug("crawling reviews of {}", storeProduct.getUrl());
        List<StoreProductReview> reviews = new ArrayList<>();
        //  url (not encoded)
        String url = storeProduct.getUrl();
        String[] arr = url.split("/");
        //  find part starting with the below prefix
        final String prefix = "dkp-";
        Optional<String> codeOpt = Arrays.stream(arr).filter(x -> x.startsWith(prefix)).findAny();
        if (codeOpt.isPresent()) {
            String codePart = codeOpt.get();
            //  remove its prefix
            String code = codePart.replace(prefix, "");
            int pageNo = 1;
            boolean hasMore;
            String reviewUrl = "";
            do {
                try {
                    reviewUrl = String.format("https://www.digikala.com/ajax/product/comments/list/%s/?page=%d&mode=buyers", code, pageNo);
                    com.jaunt.Document doc = userAgent.visit(reviewUrl);

                    com.jaunt.Elements articleElements = findEach(doc, "<div class='article'>");
                    for (com.jaunt.Element articleElement : articleElements) {
                        StoreProductReview review = new StoreProductReview();
                        review.setStoreProduct(storeProduct);

                        //  title
                        com.jaunt.Element headerElement = getFirst(articleElement, "<div class='header'>");
                        if (headerElement != null) {
                            com.jaunt.Element headerDivElemenet = getFirst(headerElement, "<div>");
                            if (headerDivElemenet != null) {
                                review.setTitle(headerDivElemenet.getTextContent());
                            }
                        }
                        //  message
                        com.jaunt.Element reviewMsgElement = getFirst(articleElement, "<p>");
                        if (reviewMsgElement != null) {
                            review.setMessage(reviewMsgElement.getTextContent());
                        }


                        //  purchased
                        com.jaunt.Element purchasedElement = findFirst(articleElement, "<div class='c-message-light c-message-light--purchased'>");
                        review.setPurchased(purchasedElement != null);

                        //  recommended
                        com.jaunt.Element recommendedElement = findFirst(articleElement, "<div class='c-message-light c-message-light--opinion-positive'>");
                        review.setRecommended(recommendedElement != null);

                        //  positive points
                        com.jaunt.Element posPointsElement = findFirst(articleElement, "<div class='c-comments__evaluation-positive'>");
                        if (posPointsElement != null) {
                            review.setPosPoints(posPointsElement.getTextContent());
                        }
                        //  nagative points
                        com.jaunt.Element negPointsElement = findFirst(articleElement, "<div class='c-comments__evaluation-negative'>");
                        if (negPointsElement != null) {
                            review.setNegPoints(negPointsElement.getTextContent());
                        }

                        //  positive votes
                        com.jaunt.Element posVotesElement = findFirst(articleElement, "<button class='btn-like js-comment-like'>");
                        if (posVotesElement != null) {
                            String posVotesStr = getAt(posVotesElement, "data-counter");
                            if (posVotesStr != null) {
                                review.setPosVotes(scrapUtils.extractNumber(posVotesStr));
                            }
                        }

                        //  negative votes
                        com.jaunt.Element negVotesElement = findFirst(articleElement, "<button class='btn-like js-comment-dislike'>");
                        if (negVotesElement != null) {
                            String negVotesStr = getAt(negVotesElement, "data-counter");
                            if (negVotesStr != null) {
                                review.setNegVotes(scrapUtils.extractNumber(negVotesStr));
                            }
                        }
                        reviews.add(review);
                    }

                    //  next button
                    com.jaunt.Elements paginations = findEach(doc, "<li class='js-pagination-item'>");
                    Iterator<com.jaunt.Element> iterator = paginations.iterator();
                    boolean activeFound = false;
                    while (iterator.hasNext()) {
                        com.jaunt.Element paginationElement = iterator.next();
                        //  find active element
                        com.jaunt.Element paginationActiveElement = findFirst(paginationElement, "<a class='c-pager__item is-active'>");
                        if (paginationActiveElement != null) {
                            activeFound = true;
                            pageNo++;
                            log.debug("Next reviews...");
                            break;
                        }
                    }
                    hasMore = activeFound;
                } catch (Exception e) {
                    log.warn("Exception fetching reviews from {}", reviewUrl);
                    hasMore = false;
                }
            } while (hasMore);
        }
        return reviews;
    }
*/

    public List<StoreProduct> getStoreProductsByJava(StoreCategory category) {
        String url = category.getUrl();
        log.info("crawling {}", url);
        List<StoreProduct> storeProducts = new ArrayList<>();
        int pageNo = 1;
        UserAgent userAgent = null;
        Date quickUpdateTryDate = new Date();
        try {
            userAgent = new UserAgent();
            while (url != null) {
                try {
                    com.jaunt.Document doc = userAgent.visit(url);
                    com.jaunt.Element firstContainer = findFirst(doc, "<section class=section list>");
                    if (firstContainer != null) {
                        List<com.jaunt.Element> containers = findEach(firstContainer, "<article class=serp-item list>").toList();
                        for (com.jaunt.Element container : containers) {
                            com.jaunt.Element productNameElement = findFirst(container, "<div class=content>");
                            if (productNameElement != null) {
                                com.jaunt.Element productNameLinkElement = findFirst(productNameElement, "<a>");

                                StoreProduct sp = new StoreProduct();
                                sp.setStore(category.getStore());

                                if (productNameLinkElement != null) {
                                    //  link
                                    String link = getAt(productNameLinkElement, "href");
                                    sp.setUrl(link);
                                    //  name
                                    String title = productNameLinkElement.getTextContent();
                                    sp.setName(title);
                                    //  price
                                    com.jaunt.Element priceElement = findFirst(container, "<p class=price>");
                                    String priceStr = priceElement == null ? null : priceElement.getTextContent();
                                    Long price = priceStr == null ? null : scrapUtils.extractNumber(priceStr);
                                    sp.setPrice(price);
                                    /*
                                    //  price before discount //not available for Sheypoor
                                    com.jaunt.Element priceBeforeDiscountElement = findFirst(container, "<del>");
                                    String priceBeforeDiscountStr = priceBeforeDiscountElement == null ? null : priceBeforeDiscountElement.getTextContent();
                                    Long priceBeforeDiscount = priceBeforeDiscountStr == null ? null : scrapUtils.extractNumber(priceBeforeDiscountStr);
                                    sp.setPriceBeforeDiscount(priceBeforeDiscount);
                                    //  stock //not available for Sheypoor
                                    com.jaunt.Element stockStatusElement = findFirst(container, "<div class='.*js-product-status.*'>");
                                    sp.setStock(stockStatusElement == null);
									*/

                                    //  update dates
                                    sp.setLastQuickUpdateTryDate(quickUpdateTryDate);
                                    sp.setLastQuickUpdateSuccessDate(new Date());
                                    storeProducts.add(sp);
                                }
                            }
                        }
                        if (containers.isEmpty()) {
                            url = null;
                        } else {
                            pageNo++;
                            url = category.getUrl() + "?pageno=" + pageNo;
                            log.info("next...");
                        }
                    } else {
                        url = null;
                    }
//                com.jaunt.Element nextElement = doc.findFirst("<a class=c-pager__next>");
                } catch (Exception e) {
                    e.printStackTrace();
                    url = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (userAgent != null) {
                try {
                    userAgent.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return storeProducts;
    }

    private com.jaunt.Element findFirst(com.jaunt.Element element, String query) {
        try {
            return element.findFirst(query);
        } catch (NotFound e) {
            return null;
        }
    }

    private com.jaunt.Element getFirst(com.jaunt.Element element, String query) {
        try {
            return element.getFirst(query);
        } catch (NotFound e) {
            return null;
        }
    }

    private com.jaunt.Elements findEach(com.jaunt.Element element, String query) {
        return element.findEach(query);
    }

    private String getAt(com.jaunt.Element element, String attribute) {
        try {
            return element.getAt(attribute);
        } catch (NotFound notFound) {
            return null;
        }
    }

    public List<StoreProduct> getStoreProductsByJsoup(StoreCategory category) {
        String url = category.getUrl();
        log.info("crawling {}", url);
        List<StoreProduct> storeProducts = new ArrayList<>();
        int pageNo = 1;
        while (url != null) {
            try {
                Connection con = Jsoup.connect(url);
                try {
                    Document doc = con.get();
                    Elements containers = doc.select(".c-listing__items > li");
                    for (Element container : containers) {
                        Element productNameElement = container.selectFirst("div.c-product-box__title > a");
                        Element priceElement = container.selectFirst("div.c-price__value-wrapper");
                        StoreProduct sp = new StoreProduct();
                        sp.setStore(category.getStore());
                        sp.setName(productNameElement.text());
                        sp.setUrl(productNameElement.attr("href"));
                        String price = priceElement == null ? null : priceElement.text();
                        sp.setPrice(price == null ? null : scrapUtils.extractNumber(price));
                        storeProducts.add(sp);
                    }
                    if (containers.isEmpty()) {
                        url = null;
                    } else {
                        pageNo++;
                        url = category.getUrl() + "?pageno=" + pageNo;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return storeProducts;
    }

    public List<StoreProduct> getStoreProductsByHtml(StoreCategory category) {
        log.info("crawling {}", category.getUrl());
        List<StoreProduct> storeProducts = new ArrayList<>();
        try (WebClient client = new WebClient(BrowserVersion.BEST_SUPPORTED)) {
            client.getOptions().setJavaScriptEnabled(false);
            client.getOptions().setCssEnabled(false);
            client.getOptions().setUseInsecureSSL(true);
            HtmlAnchor next = null;
            HtmlPage page = client.getPage(category.getUrl());
            do {
                try {
                    List<HtmlElement> list = page.getByXPath("//*[@class='c-listing__items']/li");
                    for (HtmlElement htmlElement : list) {
                        try {
                            HtmlAnchor productNameElement = htmlElement.getFirstByXPath("//div[@class='c-product-box__title']/a");
                            HtmlElement priceElmenet = htmlElement.getFirstByXPath("//div[@class='c-price__value-wrapper']");
                            String productName = productNameElement.asText();
                            String price = priceElmenet == null ? null : priceElmenet.asText();
                            StoreProduct sp = new StoreProduct();
                            sp.setStore(category.getStore());
                            sp.setName(productName);
                            sp.setPrice(price == null ? null : scrapUtils.extractNumber(price));
                            sp.setUrl(productNameElement.getHrefAttribute());
                            storeProducts.add(sp);
                        } catch (Exception e) {
                            log.warn("Exception in sheypoor: {}", category.getUrl(), e);
                        }
                    }
                    next = page.getFirstByXPath("//a[@class='button icon-right-open after next']");
                    if (next != null) {
                        page = next.click();
                    }
                } catch (IOException e) {
                    log.warn("Exception in sheypoor: {}", category.getUrl(), e);
                }
            } while (next != null);
        } catch (Exception e) {
            log.warn("Exception in sheypoor: {}", category.getUrl(), e);
        }
        return storeProducts;
    }

    public List<StoreProduct> getStoreProductsByRest(StoreCategory category) {
        log.info("Crawling category: {}", category.getUrl());
        int pageNumber = 0;
        boolean hasMore;
        List<StoreProduct> storeProducts = new ArrayList<>();
        do {
            try {
                String url = String.format("https://recommender.scarabresearch.com/merchants/123DB8D9CCA58C7C/?f=f:CATEGORY,l:100,o:%d&vc=%s", pageNumber, category.getHierarchy());
                SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
                factory.setConnectTimeout(5000);
                factory.setReadTimeout(10000);
                RestTemplate restTemplate = new RestTemplate(factory);
                ResponseEntity<SheypoorBrowseCategoryResponseDto> res = restTemplate.exchange(url, HttpMethod.GET, null, SheypoorBrowseCategoryResponseDto.class);
                if (HttpStatus.OK.equals(res.getStatusCode()) && res.getBody() != null) {
                    hasMore = res.getBody().features.CATEGORY.hasMore;
                    Map<String, SheypoorBrowseCategoryResponseDto.ProductClass> productsMap = res.getBody().products;
                    if (productsMap == null) {
                        hasMore = false;
                    } else {
                        Collection<SheypoorBrowseCategoryResponseDto.ProductClass> products = productsMap.values();
                        for (SheypoorBrowseCategoryResponseDto.ProductClass product : products) {
                            StoreProduct sp = new StoreProduct();
                            sp.setName(product.title);
                            sp.setUrl(product.link);
                            sp.setPriceBeforeDiscount(product.msrp);
                            sp.setPrice(product.price);
                            sp.setStore(category.getStore());
                            storeProducts.add(sp);
                        }
                    }
                } else {
                    hasMore = false;
                }
                pageNumber++;
            } catch (Exception e) {
                log.warn("Exception crawling sheypoor from category: {}", category.getUrl(), e);
                hasMore = false;
            }
        } while (hasMore);
        return storeProducts;
    }
}
