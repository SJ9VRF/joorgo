
# coding: utf-8

# In[43]:


import mysql.connector as sql
import pandas as pd
from sqlalchemy import create_engine
import numpy as np
import time
import statistics
from bitarray import bitarray
from bitstring import BitArray

current_milli_time = lambda: int(round(time.time() * 1000))


# In[44]:


engine = create_engine('mysql://admin:zXVcsN3n@localhost:3306/crawlerdbBk?charset=utf8mb4?collection=mb4utf8_general_ci, use_unicode=True')


# In[45]:


q1 = 'select * from product where category_id != 0xb30876375014472e95e67b61dc131844 and enabled = 1'
q2 = 'select * from product where category_id != 0xb30876375014472e95e67b61dc131844 and enabled = 0'


# In[46]:


cat_valid = 'select distinct(category_id) from product where enabled = 0 and category_id != 0xb30876375014472e95e67b61dc131844 and category_id in (select distinct(category_id) from product where enabled = 1)'


# In[47]:


df_enabled = pd.read_sql_query(q1, engine).groupby('category_id')
df_notenabled = pd.read_sql_query(q2, engine).groupby('category_id')


# In[48]:


df_cat_valid = pd.read_sql_query(cat_valid, engine)


# # Setup of database to write in similarity

# In[ ]:


import mysql.connector as sql

mydb = sql.connect(
  host="localhost",
  user="admin",
  passwd="zXVcsN3n",
  database="Matching_Result"
)

mycursor = mydb.cursor()





# In[1]:


sql = "INSERT INTO similarity_store_product (store_product_id, product_id, category_id, name_based, specification_based, brand) VALUES (s1, s2, s3, s4, s5, s6)"

sql_cat = "INSERT INTO category_computed (category_id) VALUES (cat_id)"

sql_min = "INSERT INTO similarity_store_product_min (store_product_id, product_id, category_id, name_based, specification_based, brand) VALUES (s1, s2, s3, s4, s5, s6)"


# import MySQLdb
# #connect to db
# db = MySQLdb.connect("localhost","admin","password","zXVcsN3n",'')
#  
# #setup cursor
# cursor = db.cursor()
# 
# cursor.execute(query2write)

# # Edit Distance

# In[49]:


def min_dis_two_exp(str_1, str_2) :   
    str_1_s = (str_1.replace('  ', ' ')).strip().split()
    str_2_s = (str_2.replace('  ', ' ')).strip().split()

    m = len(str_1_s)
    n = len(str_2_s)

    mat_dic = np.zeros((m, n))

    for i in range(m):
        for j in range(n):
            mat_dic[i,j] = iterative_levenshtein(str_1_s[i], str_2_s[j])
    
    return mat_dic


# In[50]:


def min_on_exp_2nd(res_mat):
    val_min = 0
    
    if len(res_mat) == 1:
        return res_mat[0][0]
    else:
        [a, b] = res_mat.shape
        while (a!=0 and b!=0):
            result = np.where(res_mat == np.amin(res_mat))
            val_min = val_min + np.amin(res_mat)
            [row, col] = [result[0][0], result[1][0]]
            res_mat = np.delete(res_mat,row,0)
            res_mat = np.delete(res_mat,col,1)
            
            [a, b] = res_mat.shape
    
    return val_min
    


# In[51]:


def iterative_levenshtein(s, t):

    rows = len(s)+1
    cols = len(t)+1
    dist = [[0 for x in range(cols)] for x in range(rows)]

    for i in range(1, rows):
        dist[i][0] = i

    for i in range(1, cols):
        dist[0][i] = i
        
    for col in range(1, cols):
        for row in range(1, rows):
            if s[row-1] == t[col-1]:
                cost = 0
            else:
                cost = 1
            dist[row][col] = np.min([dist[row-1][col] + 1, dist[row][col-1] + 1, dist[row-1][col-1] + cost])
     
    return dist[row][col]


# # On Product

# In[2]:


list_edit_dis = []
m_len = len(df_cat_valid)
for i in range(m_len):
    the_category_id = df_cat_valid.iloc[i]['category_id']
    str_cat_id = str(BitArray(the_category_id))
    gr_not_enabled_cat = df_notenabled.get_group((the_category_id))
    gr_enabled_cat = df_enabled.get_group((the_category_id))
    
    M = len(gr_not_enabled_cat)
    N = len(gr_enabled_cat)
    
    list_min_val = [] # edit distance results
    
    # iterate on not enabled products
    for J in range(M):
        
        sim_value = []
        for K in range(N):
            product_name_candid = gr_not_enabled_cat.iloc[J]['name'].replace('.', ' ').replace('  ', ' ').strip()
            product_name_main = gr_enabled_cat.iloc[K]['name'].replace('.', ' ').replace('  ', ' ').strip()
            m = len(product_name_candid.split())
            n = len(product_name_main.split())
            min_exp = min_on_exp_2nd(min_dis_two_exp(product_name_candid, product_name_main))
            
            
            min_sim = min(m,n)
            if min_sim != 0:
                sim_value.append(min_exp/(min_sim))
            else:
                sim_value.append(1000000000000)
                
            # write 2 db    
            st_product_id =  str(BitArray(gr_not_enabled_cat.iloc[J]['id']))   
            product_id = str(BitArray(gr_enabled_cat.iloc[K]['id'])) 
            category_id = str_cat_id
            name_b = str(sim_value[-1])
            
            val = (st_product_id, product_id, category_id, name_b, 'NULL', 'NULL')
            the_qu = sql.replace('s1', st_product_id).replace('s2', product_id).replace('s3', category_id).replace('s4', name_b).replace('s5','NULL').replace('s6','NULL')
            mycursor.execute(the_qu)

            mydb.commit()

            print(mycursor.rowcount, "record inserted.")    
            
        m_val = min(sim_value)
        m_ind = sim_value.index(m_val)
        
        st_product_id =  str(BitArray(gr_not_enabled_cat.iloc[J]['id']))   
        product_id = str(BitArray(gr_enabled_cat.iloc[m_ind]['id'])) 
        category_id = str_cat_id
        name_b = str(m_val)
            
        val_min = (st_product_id, product_id, category_id, name_b, 'NULL', 'NULL')
        the_qu = sql_min.replace('s1', st_product_id).replace('s2', product_id).replace('s3', category_id).replace('s4', name_b).replace('s5','NULL').replace('s6','NULL')


        mycursor.execute(the_qu)
        mydb.commit()
        
        #list_min_val.append([m_val,m_ind])  
        
        print('in category with id: '+ str_cat_id)
        print(("%.8f" % (J/M))) 

    print('in Product')
    print("%.8f" % (i/m_len)) 
    
    print('The Category with id: '+str_cat_id+ ' Finished.')
    
    val_cat = (str_cat_id)
    the_qu = sql_cat.replace('cat_id', val_cat)
    mycursor.execute(the_qu)

    mydb.commit()
    
        
    

