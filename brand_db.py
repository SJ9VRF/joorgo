
# coding: utf-8

# In[1]:


import mysql.connector as sql
import pandas as pd
from sqlalchemy import create_engine

from bitarray import bitarray
from bitstring import BitArray


# In[2]:


engine = create_engine('mysql://root:rootroot@localhost:3306/brand_new?charset=utf8mb4?collection=mb4utf8_general_ci, use_unicode=True')


# In[3]:


df_brand = pd.read_sql_table('brand', engine)


# In[4]:


def update_id_brand(id_st_pr, id_br):
    sa_qu = 'update store_product set store_product.brand_id = UUID where store_product.id = id_str and store_product.brand_id is null;'
    sa_qu_res = sa_qu.replace('UUID',id_br).replace('id_str',id_st_pr)
    
    return sa_qu_res


# In[5]:


# and brand_id is null
sam_que = "select id from store_product_brand where brand like '%%BRNAME%%'"


# In[ ]:


brand_pr = []
m = len(df_brand)
for j in range(m):
    for i in range(1,5):
        brandName = df_brand.iloc[j][i].strip()
        brandName = brandName.replace('\'', '\\\'')
        if len(brandName)>0:
            the_query = sam_que.replace('BRNAME', brandName)
            df_id_pr = pd.read_sql_query(the_query,engine)
            brand_pr.append([df_id_pr, df_brand.iloc[j][0]])
            
    print(j/m)


# In[10]:


the_query = []
for item in brand_pr:
    brand_id = str(BitArray(item[1]))
    item_id = item[0]
    for jtem in item_id['id']:
        id_str_pr = str(BitArray(jtem))
        the_query.append(update_id_brand(id_str_pr, brand_id))


# In[11]:


add_brand_qu = '/Users/arefeh/Desktop/setBrand.sql'


# In[149]:


with open(add_brand_qu, 'w') as f:
    for item in the_query:
        f.write(item+'\n')

