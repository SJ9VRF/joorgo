
# coding: utf-8

# In[50]:


import nltk
import spacy
spacy.load('en')
from spacy.lang.en import English
parser = English()


# In[51]:


import nltk
nltk.download('wordnet')
from nltk.corpus import wordnet as wn
def get_lemma(word):
    lemma = wn.morphy(word)
    if lemma is None:
        return word
    else:
        return lemma
    
from nltk.stem.wordnet import WordNetLemmatizer
def get_lemma2(word):
    return WordNetLemmatizer().lemmatize(word)


# In[52]:


nltk.download('stopwords')
en_stop = set(nltk.corpus.stopwords.words('english'))


# In[53]:


def tokenize(text):
    lda_tokens = []
    tokens = parser(text)
    for token in tokens:
        if token.orth_.isspace():
            continue
        elif token.like_url:
            lda_tokens.append('URL')
        elif token.orth_.startswith('@'):
            lda_tokens.append('SCREEN_NAME')
        else:
            lda_tokens.append(token.lower_)
    return lda_tokens


# In[54]:


def prepare_text_for_lda(text):
    tokens = tokenize(text)
    tokens = [token for token in tokens if len(token) > 4]
    tokens = [token for token in tokens if token not in en_stop]
    tokens = [get_lemma(token) for token in tokens]
    return tokens


# In[55]:


from gensim import corpora
dictionary = corpora.Dictionary(text_data)
corpus = [dictionary.doc2bow(text) for text in text_data]
import pickle
pickle.dump(corpus, open('corpus.pkl', 'wb'))
dictionary.save('dictionary.gensim')


# In[56]:


add_input = '/Users/arefeh/Desktop/dataset.csv'
text_data = []
with open(add_input, 'r') as f:
    for line in f:
        tokens = prepare_text_for_lda(line)
        text_data.append(tokens)


# In[57]:


import gensim
NUM_TOPICS = 5
ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics = NUM_TOPICS, id2word=dictionary, passes=15)
ldamodel.save('model5.gensim')

topics = ldamodel.print_topics(num_words=10)
#for topic in topics:
#    print(topic)


# In[58]:


def wei_wor(topics):    
    weight = []
    word = []
    num_top = 5
    for i in range(num_top):
        elements_topic = topics[i][1].split('+')
        num_wor = len(elements_topic)
        for j in range(num_wor):
            weight.append(float(elements_topic[j].split('*')[0]))
            word.append((elements_topic[j].split('*')[1]))
    return ([word, weight])


# In[59]:


result = wei_wor(topics)


# In[60]:


import numpy as np


# In[61]:


def score_topics(topic_res, doc):
    score_item = []
    for item in doc:
        n = len(topic_res[0])
        score = 0.0
        
        for i in range(n):
            tword = topic_res[0][i]
            tweig = topic_res[1][i]
            score = score + item.count(tword.strip().replace('"',''))*tweig
        m = len(item.split())
        if m != 0:
            score_item.append(score/m)
        else:
            score_item.append(0)
    
    sum_top = []
    
    score_item_np = np.array(score_item)
    index_sort = np.argsort(-score_item_np)

    
    n_desc = min(5, int(len(doc)/3))
    
    twic_rep = min(2*n_desc, len(doc))
    
    index_sort_s = np.sort(index_sort[:twic_rep])
    
    len_ind = len(index_sort_s)
    
    n_ss = len(index_sort_s)
    i_ss = 0
    while(i_ss<n_ss)    :
        item = index_sort_s[i_ss]
        if (item+1) in index_sort_s:
            index_sort_s_new = np.delete(index_sort_s, np.where(index_sort_s == item+1))  
            index_sort_s = index_sort_s_new 
        n_ss = len(index_sort_s)
        i_ss = i_ss+1
                 
            
    un_rep_score = []
    un_rep_score_ind = []
    for item in index_sort_s:
        un_rep_score.append(score_item_np[item])
        un_rep_score_ind.append(item)
        
    
    index_sort_2 = np.argsort(-np.array(un_rep_score))
    n_desc_2 = min(5, int(len(doc)/3), len(index_sort_2))
    index_sort_s_2 = np.sort(index_sort_2[:n_desc_2])
    
    sum_top = []
    for i in range(n_desc_2):
        sum_top.append(doc[un_rep_score_ind[index_sort_s_2[i]]])
    
    
    return sum_top


# In[62]:


import random
text_doc = []
with open('/Users/arefeh/Desktop/dataset.csv') as f:
    text_doc = f.readlines()


# In[63]:


text_doc = text_doc[0].split('. ')


# In[64]:


sum_de = score_topics(result, text_doc)


# In[65]:


sum_add = '/Users/arefeh/Desktop/sumTopic.txt'
with open (sum_add, 'w') as f:
    for item in sum_de:
        f.write(item+'.'+'\n'+'\n')

