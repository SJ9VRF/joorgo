
# coding: utf-8

# In[1]:


import mysql.connector as sql
import pandas as pd
from sqlalchemy import create_engine
import numpy as np
import time
import statistics
from bitarray import bitarray
from bitstring import BitArray


# In[2]:


engine = create_engine('mysql://admin:zXVcsN3n@localhost:3306/crawlerdbBk?charset=utf8mb4?collection=mb4utf8_general_ci, use_unicode=True')


# In[3]:


q1 = 'select * from product where category_id != 0xb30876375014472e95e67b61dc131844 and enabled = 1'
q2 = 'select * from product where category_id != 0xb30876375014472e95e67b61dc131844 and enabled = 0'


# In[4]:


cat_valid = 'select distinct(category_id) from product where enabled = 0 and category_id != 0xb30876375014472e95e67b61dc131844 and category_id in (select distinct(category_id) from product where enabled = 1)'


# In[ ]:


df_enabled = pd.read_sql_query(q1, engine).groupby('category_id')
df_notenabled = pd.read_sql_query(q2, engine).groupby('category_id')


# In[ ]:


df_cat_valid = pd.read_sql_query(cat_valid, engine)


# # Edit Distance on Spec

# In[6]:


def min_dis_two_exp(str_1, str_2) :   
    str_1_s = (str_1.replace('  ', ' ')).strip().split()
    str_2_s = (str_2.replace('  ', ' ')).strip().split()

    m = len(str_1_s)
    n = len(str_2_s)

    mat_dic = np.zeros((m, n))

    for i in range(m):
        for j in range(n):
            mat_dic[i,j] = iterative_levenshtein(str_1_s[i], str_2_s[j])
    
    return mat_dic


# In[7]:


def min_on_exp_2nd(res_mat):
    val_min = 0
    
    if len(res_mat) == 1:
        return res_mat[0][0]
    else:
        [a, b] = res_mat.shape
        while (a!=0 and b!=0):
            result = np.where(res_mat == np.amin(res_mat))
            val_min = val_min + np.amin(res_mat)
            [row, col] = [result[0][0], result[1][0]]
            res_mat = np.delete(res_mat,row,0)
            res_mat = np.delete(res_mat,col,1)
            
            [a, b] = res_mat.shape
    
    return val_min
    


# In[8]:


def iterative_levenshtein(s, t):

    rows = len(s)+1
    cols = len(t)+1
    dist = [[0 for x in range(cols)] for x in range(rows)]

    for i in range(1, rows):
        dist[i][0] = i

    for i in range(1, cols):
        dist[0][i] = i
        
    for col in range(1, cols):
        for row in range(1, rows):
            if s[row-1] == t[col-1]:
                cost = 0
            else:
                cost = 1
            dist[row][col] = np.min([dist[row-1][col] + 1, dist[row][col-1] + 1, dist[row-1][col-1] + cost])
     
    return dist[row][col]


# In[16]:


def pre_Process(inputString):
    cleanString = inputString.replace('.',' ')
    cleanString = cleanString.replace('-',' ')
    cleanString = cleanString.replace('_',' ')
    cleanString = cleanString.replace('(',' ')
    cleanString = cleanString.replace(')',' ')
    cleanString = cleanString.replace('[',' ')
    cleanString = cleanString.replace(']',' ')
    cleanString = cleanString.replace('{',' ')
    cleanString = cleanString.replace('}',' ')
    
    cleanString = lower(cleanString)
    
    cleanString = cleanString.replace('  ',' ')
    cleanString = cleanString.strip()
    
    return cleanString


# In[17]:


def com_sim_spec(cand_spec, main_spec):
    M = len(cand_spec)
    N = len(main_spec)
    
    val_w = 1
    titl_w = 0.6 
    
    mat_val = np.zeros((M,N))
    
    for i in range(M):
        ctem_title = pre_Process(cand_spec.iloc[i]['title'])
        ctem_value = pre_Process(cand_spec.iloc[i]['value'])
        
        for j in range(N):
            mtem_title = pre_Process(main_spec.iloc[j]['title'])
            mtem_value = pre_Process(main_spec.iloc[j]['value'])     
            
            val_tit = min_on_exp_2nd(min_dis_two_exp(ctem_title, mtem_title))
            val_val = min_on_exp_2nd(min_dis_two_exp(ctem_value, mtem_value))
            
            mat_val[i][j] = (val_w*val_val + titl_w*val_tit)/2
    
    return (min_on_exp_2nd(mat_val))


# # On Product

# In[ ]:


list_edit_dis = []
m_len = len(df_cat_valid)
for i in range(m_len):
    the_category_id = df_cat_valid.iloc[i]['category_id']
    gr_not_enabled_cat = df_notenabled.get_group((the_category_id))
    gr_enabled_cat = df_enabled.get_group((the_category_id))
    
    M = len(gr_not_enabled_cat)
    N = len(gr_enabled_cat)
    
    list_min_val = [] # edit distance results
    
    query_s_v = 'select title, value from product_specification where product_specification.product_id = PrId;'
    
    # iterate on not enabled products
    for J in range(M):
        
        id_cand = str(BitArray(gr_not_enabled_cat.iloc[J]['id']))
        df_spec_candid = pd.read_sql_query(query_s_v.replace('PrId', id_cand), engine)
        
        sim_value = []
        for K in range(N):
            
            id_main =  str(BitArray(gr_enabled_cat.iloc[K]['id']))
            df_spec_main = pd.read_sql_query(query_s_v.replace('PrId', id_main), engine)
            
            similarity_spec = com_sim_spec(df_spec_candid, df_spec_main)
                   
            m = len(df_spec_candid)
            n = len(df_spec_main)

            min_sim = min(m,n)
            if min_sim != 0:
                sim_value.append(similarity_spec/(min_sim))
            else:
                sim_value.append(1000000000000)
        
        m_val = min(sim_value)
        m_ind = sim_value.index(m_val)
        
        print('in category')
        print(("%.8f" % (J/M))) 
    
    print('in Product')
    print("%.8f" % (i/m_len))        
    list_min_val.append([m_val,m_ind])  


# # Analysis

# In[ ]:


Result_list = []
n = len(list_min_val)
for i in range(n):
    to_match_id = str(BitArray(gr_not_enabled_cat.iloc[i]['id']))
    main_pr_id = str(BitArray(gr_enabled_cat.iloc[i]['id']))
    Result_list.append([to_match_id, main_pr_id, list_min_val[i][0]])


# In[24]:


add_qu = '/home/user/yavary/match_spec.sql'


# In[25]:


with open (add_qu, 'w') as f:
    for item in Result_list:
        f.write(str(item[0])+','+str(item[1])+','+str(item[2])+'\n')


# # Write on DB
