# coding: utf-8

# In[43]:


import mysql.connector as sql
import pandas as pd
from sqlalchemy import create_engine
import numpy as np
import time
import statistics
from bitarray import bitarray
from bitstring import BitArray

current_milli_time = lambda: int(round(time.time() * 1000))


# In[44]:


engine = create_engine('mysql://root:rootroot@localhost:3306/matching_name_light?charset=utf8mb4?collection=mb4utf8_general_ci, use_unicode=True')


# In[45]:


q1 = 'select * from product where category_id != 0xb30876375014472e95e67b61dc131844 and enabled = 1'
q2 = 'select * from product where category_id != 0xb30876375014472e95e67b61dc131844 and enabled = 0'


# In[46]:


cat_valid = 'select distinct(category_id) from product where enabled = 0 and category_id != 0xb30876375014472e95e67b61dc131844 and category_id in (select distinct(category_id) from product where enabled = 1)'


# In[47]:


df_enabled = pd.read_sql_query(q1, engine).groupby('category_id')
df_notenabled = pd.read_sql_query(q2, engine).groupby('category_id')


# In[48]:


df_cat_valid = pd.read_sql_query(cat_valid, engine)


# # Edit Distance

# In[49]:


def min_dis_two_exp(str_1, str_2) :   
    str_1_s = (str_1.replace('  ', ' ')).strip().split()
    str_2_s = (str_2.replace('  ', ' ')).strip().split()

    m = len(str_1_s)
    n = len(str_2_s)

    mat_dic = np.zeros((m, n))

    for i in range(m):
        for j in range(n):
            mat_dic[i,j] = iterative_levenshtein(str_1_s[i], str_2_s[j])
    
    return mat_dic


# In[50]:


def min_on_exp_2nd(res_mat):
    val_min = 0
    
    if len(res_mat) == 1:
        return res_mat[0][0]
    else:
        [a, b] = res_mat.shape
        while (a!=0 and b!=0):
            result = np.where(res_mat == np.amin(res_mat))
            val_min = val_min + np.amin(res_mat)
            [row, col] = [result[0][0], result[1][0]]
            res_mat = np.delete(res_mat,row,0)
            res_mat = np.delete(res_mat,col,1)
            
            [a, b] = res_mat.shape
    
    return val_min
    


# In[51]:


def iterative_levenshtein(s, t):

    rows = len(s)+1
    cols = len(t)+1
    dist = [[0 for x in range(cols)] for x in range(rows)]

    for i in range(1, rows):
        dist[i][0] = i

    for i in range(1, cols):
        dist[0][i] = i
        
    for col in range(1, cols):
        for row in range(1, rows):
            if s[row-1] == t[col-1]:
                cost = 0
            else:
                cost = 1
            dist[row][col] = np.min([dist[row-1][col] + 1, dist[row][col-1] + 1, dist[row-1][col-1] + cost])
     
    return dist[row][col]


# # On Product

# In[52]:


list_edit_dis = []
m_len = len(df_cat_valid)
for i in range(m_len):
    the_category_id = df_cat_valid.iloc[i]['category_id']
    gr_not_enabled_cat = df_notenabled.get_group((the_category_id))
    gr_enabled_cat = df_enabled.get_group((the_category_id))
    
    M = len(gr_not_enabled_cat)
    N = len(gr_enabled_cat)
    
    list_min_val = [] # edit distance results
    
    # iterate on not enabled products
    for J in range(M):
        
        sim_value = []
        for K in range(N):
            product_name_candid = gr_not_enabled_cat.iloc[J]['name'].replace('.', ' ').replace('  ', ' ').strip()
            product_name_main = gr_enabled_cat.iloc[K]['name'].replace('.', ' ').replace('  ', ' ').strip()
            m = len(product_name_candid.split())
            n = len(product_name_main.split())
            min_exp = min_on_exp_2nd(min_dis_two_exp(product_name_candid, product_name_main))
            sim_value.append(min_exp/(min([m,n])))
        m_val = min(sim_value)
        m_ind = sim_value.index(m_val)
        
        #print('that')
        #print(("%.2f" % (J/M))) 
    
    #print('this')
    #print("%.2f" % (i/m_len))        
    list_min_val.append([m_val,m_ind])  


# # Analysis

# In[53]:


the_computed_thereshod = 0.5


# In[54]:


New_list = []
Match_list = []
n = len(list_min_val)
for i in range(n):
    if list_min_val[i][0]<the_computed_thereshod:
        to_match_id = str(BitArray(gr_not_enabled_cat.iloc[i]['id']))
        main_pr_id = str(BitArray(gr_enabled_cat.iloc[i]['id']))
        Match_list.append([to_match_id, main_pr_id])
    else:
        new_pr_id = str(BitArray(gr_not_enabled_cat.iloc[i]['id']))
        New_list.append(new_pr_id)


# Some Definitions:
#     gr_not_enabled_cat: these are products which are matching target

# # Create Query

# In[55]:


#new product
def new_product(new_id):
    query = 'Update product set product.enabled = 1 where product.id = NEWPR;'
    
    return (query.replace('NEWPR',new_id))


# In[56]:


#match product
def match_product(match_id, main_id):
    qu_st_pr = 'Update store_product set store_product.product_id = MainID where store_product.product_id = MatchID;'
    qu_pr = 'Update product set product.main_store_product_id = NULL where product.id = match_id;'
    
    qu_st_str = qu_st_pr.replace(MainID, main_id).replace(MatchID, match_id)
    qu_pr_str = qu_pr.replace(MatchID, match_id)
    
    return ([qu_st_str, qu_pr_str])


# In[61]:


query_match_run = []
for item in New_list:
    query_match_run.append(new_product(item))
    
for item in Match_list:
    qtem = match_product(item)
    query_match_run.append(qtem[0])
    query_match_run.append(qtem[1])
    
delete_pr = 'delete from product where product.main_store_product_id is NULL;'        
query_match_run.append(delete_pr)


# In[62]:


add_qu = '/users/arefeh/Desktop/match.sql'


# In[63]:


with open (add_qu, 'w') as f:
    f.writelines( list( "%s\n" % item for item in query_match_run ) )

