import java.io.File;

import org.apache.lucene.search.spell.PlainTextDictionary;
import org.apache.lucene.search.spell.SpellChecker;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class SimpleSuggestionService {

    public static void main(String[] args) throws Exception {

        String path_Dic = new String("Users/arefeh/desktop/Dic");

        File dir = new File(path_Dic);

        Directory directory = FSDirectory.open(dir);

        SpellChecker spellChecker = new SpellChecker(directory);

        String path_sampleDic_file = new String("Users/arefeh/desktop/sampleDictionary.txt");

        spellChecker.indexDictionary(
                new PlainTextDictionary(new File(path_sampleDic_file)));

        String wordForSuggestions = "hwllo";

        int suggestionsNumber = 5;

        String[] suggestions = spellChecker.
                suggestSimilar(wordForSuggestions, suggestionsNumber);

        if (suggestions!=null && suggestions.length>0) {
            for (String word : suggestions) {
                System.out.println("Did you mean:" + word);
            }
        }
        else {
            System.out.println("No suggestions found for word:"+wordForSuggestions);
        }

    }

}