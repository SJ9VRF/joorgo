#دسته‌بندی‌های محاسبه شده در الگوریتم تطابق بر اساس نام
Select * from Matching_Result.category_computed;
#دسته‌بندی‌های محاسبه شده در الگوریتم تطابق بر اساس مشخصات
Select * from Matching_Result.category_computed_spec;


#بر اساس نام
#تعداد تطبیق داده شده تعداد پروداکت‌ها
Select distinct(product_id) from Matching_Result.similarity_store_product group by product_id where name_based < (  select Matching_Result.th from Matching_Result.th.thereshod where Matching_Result.th.category_id = Matching_Result.similarity_store_product.category_id  )

#+فلگ دیجی
#تعداد استورپروداکت ب ازای هر پروداکت تطبیق داده شده
Select count(distinct Matching_Result.similarity_store_product￼.store_product_id), (Matching_Result.similarity_store_product￼.digi_flag) from Matching_Result.similarity_store_product￼ Group by product_id where name_based < ( select Matching_Result.th from Matching_Result.th.thereshod where Matching_Result.th.category_id = Matching_Result.similarity_store_product.category_id ) group by Matching_Result.similarity_store_product￼.product_id;
#
#تعداد تطبیق داده نشده تعداد استورپروداکت‌ها
Select count(distinct Matching_Result.similarity_store_product￼.store_product_id), from Matching_Result.similarity_store_product￼ Group by product_id where name_based >= ( select Matching_Result.th from Matching_Result.th.thereshod where Matching_Result.th.category_id = Matching_Result.similarity_store_product.category_id ) group by Matching_Result.similarity_store_product￼.product_id;



#بر اساس مشخصات
#تعداد تطبیق داده شده تعداد پروداکت‌ها
Select distinct(product_id) from Matching_Result.similarity_store_product_spec group by product_id where name_based < (  select Matching_Result.th from Matching_Result.th.thereshod where Matching_Result.th.category_id = Matching_Result.similarity_store_product_spec.category_id  )

#+فلگ دیجی
#تعداد استورپروداکت ب ازای هر پروداکت تطبیق داده شده
Select count(distinct Matching_Result.similarity_store_product_spec￼.store_product_id), (Matching_Result.similarity_store_product_spec￼.digi_flag) from Matching_Result.similarity_store_product_spec￼ Group by product_id where name_based < ( select Matching_Result.th from Matching_Result.th.thereshod where Matching_Result.th.category_id = Matching_Result.similarity_store_product_spec.category_id ) group by Matching_Result.similarity_store_product_spec￼.product_id;
#
#تعداد تطبیق داده نشده تعداد استورپروداکت‌ها
Select count(distinct Matching_Result.similarity_store_product_spec￼.store_product_id), from Matching_Result.similarity_store_product_spec￼ Group by product_id where name_based >= ( select Matching_Result.th from Matching_Result.th.thereshod where Matching_Result.th.category_id = Matching_Result.similarity_store_product_spec.category_id ) group by Matching_Result.similarity_store_product_spec￼.product_id;